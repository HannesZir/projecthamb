const Discord = require("discord.js");
const client = new Discord.Client();
const fs = require("fs");

const config = require('./config.json');


//liest den /events/ ordner und verküpft jede event-datei mit dem entsprechendem event.
fs.readdir("./events/", (err,files) => {
  if(err) return console.error(err);

  files.forEach(file => {
    let eventFunction = require('./events/'+ file +'');
    let eventName = file.split(".")[0];

    //eventaufruf mit allen dazugehörigen Argumenten
    client.on(eventName, (...args) => eventFunction.run(client, ...args));
  })
})

client.on("message", (message) => {
  //exit and stop if theres no prefix
  if (!message.content.startsWith(config.prefix) || message.author.bot) return;
  
  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  console.log(command);


  try {
    let commandFile = require('./commands/'+ command +'.js');
    commandFile.run(client, message, args);
  } catch(err) {
    
    console.error(err);
  }
});

client.login(config.token);