const config = require("../config.json");
const fs = require("fs");

exports.run = (client, message, args) => {
  if(message.author.id !== config.ownerID) return;

  let newPrefix = args[0];
  config.prefix = newPrefix;

  fs.writeFile("../config.json", JSON.stringify(config), (err) => console.error);
  message.channel.send("Prefix changed to: " + newPrefix);
}